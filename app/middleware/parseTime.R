# 時間の文字列をマイクロ秒数のintにする
# 例: "0:47:06.15" => (3600*0 + 60*47 + 6.15)*1000
parseTime.toMs <- function(strTime) {
  t = strsplit(strTime, ":")
  t_num = as.numeric(t[[1]])
  h = as.integer(t_num[[1]]*3600)
  m = as.integer(t_num[[2]]*60)
  s = as.integer(t_num[3]*10^3)
  if(h<0) 0 else (h + m)*10^3 + s
}

# ミリ秒intを時間を表す文字列にする
# 例: (3600*0 + 60*47 + 6.15)*1000 => "0:47:06.15"
parseTime.toStr <- function(msTime) {
  t = msTime / 1000
  h = as.integer(t / 3600)
  t = t %% 3600

  m = as.integer(t / 60)
  if (m < 10) { m = paste("0", m, sep="") }
  t = t %% 60

  s = round(t, digits=2)
  if (s < 10) { s = paste("0", s, sep="") }

  paste(h, m, s, sep=":")
}
